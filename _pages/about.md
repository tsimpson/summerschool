---
layout: about
title: about
permalink: /
subtitle: hosted by the <a href='https://web.inf.ed.ac.uk/cdt/biomedical-ai'>UKRI Centre for Doctoral Training in Biomedical AI</a> at  the <a href='https://www.inf.ed.ac.uk'>School of Informatics</a>, <a href='https://www.ed.ac.uk'>The University of Edinburgh</a>. <p><b> 17th - 18th July 2023 - Edinburgh, UK</b>.</p>

banner:
  image: EdinburghUniSalisburyCrags.jpg

profile:
  align: center
  address: >
    <p>555 your office number</p>
    <p>123 your address street</p>
    <p>Your City, State 12345</p>

news: true  # includes a list of news items
selected_papers: false # includes a list of papers marked as "selected={true}"
social: true  # includes social icons at the bottom of the page
---

Write your biography here. Tell the world about yourself. Link to your favorite [subreddit](http://reddit.com). You can put a picture in, too. The code is already in, just name your picture `prof_pic.jpg` and put it in the `img/` folder.

Put your address / P.O. box / other info right below your picture. You can also disable any these elements by editing `profile` property of the YAML header of your `_pages/about.md`. Edit `_bibliography/papers.bib` and Jekyll will render your [publications page](/al-folio/publications/) automatically.

Link to your social media connections, too. This theme is set up to use [Font Awesome icons](http://fortawesome.github.io/Font-Awesome/) and [Academicons](https://jpswalsh.github.io/academicons/), like the ones below. Add your Facebook, Twitter, LinkedIn, Google Scholar, or just disable all of them.
