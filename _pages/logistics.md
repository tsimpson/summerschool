---
layout: page
title: logistics
nav: true
nav_order: 6
dropdown: true
children: 
    - title: registration
      permalink: /registration/
    - title: divider
    - title: travel
      permalink: /travel/
    - title: divider
    - title: accommodation
      permalink: /accommodation/
---