---
layout: post
date: 2023-03-22 00:00:00
inline: true
---

Please **register your interest** in the summer school by visiting [this page](/summerschool/registration). We will contact you when registration is open. Do check back on the website regularly. You can also contact us by [e-mail](mailto:biomedai-cdt@inf.ed.ac.uk).
